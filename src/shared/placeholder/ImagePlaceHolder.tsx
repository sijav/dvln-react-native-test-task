import React from 'react';
import {Rect} from 'react-native-svg';
import SvgAnimatedLinearGradient from 'react-native-svg-animated-linear-gradient';
import {useScreenViewSize} from 'src/shared/utils/useScreenViewSize';

function ImagePlaceHolderItem() {
  const {screenHeight, screenWidth} = useScreenViewSize();
  const maxSize = Math.min(screenHeight, screenWidth / 2, 220);
  return (
    <SvgAnimatedLinearGradient height={maxSize} width={maxSize * 2}>
      <Rect x="10" y="10" rx="5" ry="5" width={maxSize - 20} height={maxSize - 20} />
      <Rect x={maxSize + 10} y="10" rx="5" ry="5" width={maxSize - 20} height={maxSize - 20} />
    </SvgAnimatedLinearGradient>
  );
}

interface OwnProps {
  count: number;
}

type Props = OwnProps;

export function ImagePlaceHolder(props: Props) {
  return (
    <>
      {new Array(props.count).fill(null).map((item, index) => (
        <ImagePlaceHolderItem key={index} />
      ))}
    </>
  );
}
