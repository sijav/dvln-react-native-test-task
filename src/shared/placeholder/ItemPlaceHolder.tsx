import {LayoutChangeEvent, View} from 'react-native';
import React, {useCallback, useState} from 'react';

import {Rect} from 'react-native-svg';
import SvgAnimatedLinearGradient from 'react-native-svg-animated-linear-gradient';

export const ItemPlaceHolderItem = () => {
  const [layout, setLayout] = useState({width: 0, height: 0});
  const handleOnLayout = useCallback((event: LayoutChangeEvent) => {
    const {height, width} = event.nativeEvent.layout;
    setLayout({height, width});
  }, []);
  return (
    <View onLayout={handleOnLayout}>
      <SvgAnimatedLinearGradient width={layout.width} height={51}>
        <Rect x="20" y="20" rx="5" ry="5" width={layout.width - 80} height={25} />
      </SvgAnimatedLinearGradient>
    </View>
  );
};

interface OwnProps {
  count: number;
}

type Props = OwnProps;

export function ItemPlaceHolder(props: Props) {
  return (
    <>
      {new Array(props.count).fill(null).map((item, index) => (
        <ItemPlaceHolderItem key={index} />
      ))}
    </>
  );
}
