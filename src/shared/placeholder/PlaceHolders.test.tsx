import 'react-native';

import renderer, {ReactTestRenderer, act} from 'react-test-renderer';

import {ImagePlaceHolder} from './ImagePlaceHolder';
import {ItemPlaceHolder} from './ItemPlaceHolder';
import React from 'react';
import {getRenderedRoot} from 'src/shared/test-utils/renderedUtils';

describe('<ImagePlaceHolder count={8} />', () => {
  it('should render correctly and have exact 8 children', async () => {
    let rendered: ReactTestRenderer | null = null;

    act(() => {
      rendered = renderer.create(<ImagePlaceHolder count={8} />);
    });

    const root = getRenderedRoot(rendered);
    const childrens = root.children;
    expect(childrens?.length).toEqual(8);
  });
});

describe('<ItemPlaceHolder count={8} />', () => {
  it('should render correctly and have exact 8 children', async () => {
    let rendered: ReactTestRenderer | null = null;

    await act(async () => {
      rendered = renderer.create(<ItemPlaceHolder count={8} />);
    });

    const root = getRenderedRoot(rendered);
    const childrens = root.children;
    expect(childrens?.length).toEqual(8);
  });
});
