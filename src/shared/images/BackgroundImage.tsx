import {CSSObject, StyledComponentInnerOtherProps, StyledComponentPropsWithRef} from 'styled-components';

import React from 'react';
import styled from 'styled-components/native';
import {useWindowDimensions} from 'react-native';

type StyledBackgroundImageProps = {
  windowWidth: number;
  windowHeight: number;
};

export const StyledBackgroundImage = styled.Image(
  (props: StyledBackgroundImageProps) =>
    ({
      position: 'absolute',
      width: props.windowWidth,
      height: props.windowHeight,
    } as CSSObject),
);

type BackgroundImageProps = Exclude<
  StyledComponentPropsWithRef<typeof StyledBackgroundImage>,
  StyledComponentInnerOtherProps<typeof StyledBackgroundImage>
>;

export function BackgroundImage(props: BackgroundImageProps) {
  const window = useWindowDimensions();
  return <StyledBackgroundImage windowWidth={window.width} windowHeight={window.height} {...props} />;
}
