import 'react-native';

import {Image, ScaledSize, useWindowDimensions} from 'react-native';
import renderer, {ReactTestRenderer, act} from 'react-test-renderer';

import {BackgroundImage} from './BackgroundImage';
import React from 'react';
import {getRenderedRoot} from 'src/shared/test-utils/renderedUtils';
import logo from 'src/assets/images/logo.png';

describe('<BackgroundImage source={logo} />', () => {
  it('should render correctly and have source logo', async () => {
    let rendered: ReactTestRenderer | null = null;
    let window: ScaledSize | null = null;
    const BackgroundImageTestComponent = () => {
      window = useWindowDimensions();
      return <BackgroundImage source={logo} />;
    };

    await act(async () => {
      rendered = renderer.create(<BackgroundImageTestComponent />);
    });

    const root = getRenderedRoot(rendered);
    const child = root.findByProps({source: logo}).findByType(Image);

    expect(child.props?.windowWidth).toEqual(((window as unknown) as ScaledSize).width);
    expect(child.props?.windowHeight).toEqual(((window as unknown) as ScaledSize).height);
  });
});
