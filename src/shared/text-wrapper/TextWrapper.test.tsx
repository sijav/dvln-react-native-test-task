import 'react-native';

import renderer, {ReactTestRenderer, act} from 'react-test-renderer';

import React from 'react';
import {TextWrapper} from './TextWrapper';
import {getRenderedTree} from 'src/shared/test-utils/renderedUtils';

describe('<TextWrapper />', () => {
  it('should render correctly', async () => {
    let rendered: ReactTestRenderer | null = null;

    await act(async () => {
      rendered = renderer.create(<TextWrapper />);
    });

    expect(getRenderedTree(rendered)).toBeTruthy();
  });
});
