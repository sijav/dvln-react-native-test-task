import {Action} from 'redux';
import {ThunkAction} from 'redux-thunk';

export type CommonThunkAction<RootState> = ThunkAction<void, RootState, string, Action<string>>;
