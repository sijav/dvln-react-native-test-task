import {ImageInterface} from 'src/core/interfaces';
export interface NavigationProps extends Record<string, object | undefined> {
  Home?: {id: number};
  Image: {image: ImageInterface};
}
