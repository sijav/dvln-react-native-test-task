import axios from 'axios';
import axiosMockAdapter from 'axios-mock-adapter';
// this file is for axiosMockAdapter bug https://github.com/ctimmerm/axios-mock-adapter/issues/116

axios.defaults.baseURL = 'http://evil';
export const mockAxios = new axiosMockAdapter(axios);
