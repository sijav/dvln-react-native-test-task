export const createTestProps = (props: Object) =>
  ({
    navigation: {
      navigate: jest.fn(),
      replace: jest.fn(),
    },
    route: {
      params: {},
    },
    ...props,
  } as any);
