import {ReactTestRenderer} from 'react-test-renderer';

// these method is for typescript solution that rendered happens in act but ts doesn't understands it
export const getRenderedTree = (rendered: null) => ((rendered as unknown) as ReactTestRenderer).toTree();

// these method is for typescript solution that rendered happens in act but ts doesn't understands it
export const getRenderedRoot = (rendered: null) => ((rendered as unknown) as ReactTestRenderer).root;
