export const calculateIsLargeScreen = (width: number) => width >= 768;
