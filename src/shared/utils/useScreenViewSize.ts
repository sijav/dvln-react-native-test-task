import {Platform, useWindowDimensions} from 'react-native';

import {calculateIsLargeScreen} from './calculateIsLargeScreen';

export function useScreenViewSize() {
  const {width, height} = useWindowDimensions();
  const smallerAxisSize = Math.min(height, width);
  const isTablet = smallerAxisSize >= 600;
  const isLandscape = width > height;
  const appBarHeight = Platform.OS === 'ios' ? (isLandscape ? 32 : 44) : 56;
  const maxDrawerWidth = isTablet ? 320 : 280;
  const drawerWidth = Math.min(smallerAxisSize - appBarHeight, maxDrawerWidth);
  // above code has been copied from DrawerNavigator.js
  const isLargeScreen = calculateIsLargeScreen(width);
  const screenWidth = isLargeScreen ? width - drawerWidth : width;
  const screenHeight = height - appBarHeight;
  return {screenWidth, screenHeight, width, height, isTablet, isLandscape, appBarHeight, drawerWidth, isLargeScreen};
}
