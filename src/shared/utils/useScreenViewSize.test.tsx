import 'react-native';

import {Text, View} from 'react-native';

import React from 'react';
// Note: test renderer must be required after react-native.
import ShallowRenderer from 'react-test-renderer/shallow';
import {useScreenViewSize} from './useScreenViewSize';

const TestScreenViewSizes = () => {
  const sizes = useScreenViewSize();
  return (
    <View>
      <Text>{JSON.stringify(sizes)}</Text>
    </View>
  );
};

const renderer = ShallowRenderer.createRenderer();

describe('useScreenViewSize', () => {
  it('gives the correct values', () => {
    renderer.render(<TestScreenViewSizes />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
