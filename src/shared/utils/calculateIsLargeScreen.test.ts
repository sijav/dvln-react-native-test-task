import {calculateIsLargeScreen} from './calculateIsLargeScreen';

describe('calculateIsLargeScreen', () => {
  it('should states that under 700 is not a large screen', () => {
    expect(calculateIsLargeScreen(700)).toBeFalsy();
  });
});
