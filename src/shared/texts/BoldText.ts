import styled from 'styled-components/native';

export const BoldText = styled.Text`
  fontWeight: bold
  fontSize: 20px
`;
