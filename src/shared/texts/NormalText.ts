import styled from 'styled-components/native';

export const NormalText = styled.Text`
  fontSize: 18px
  margin: 5px
  textAlign: center
`;
