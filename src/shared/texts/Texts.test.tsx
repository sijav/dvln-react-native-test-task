import 'react-native';

import renderer, {ReactTestRenderer, act} from 'react-test-renderer';

import {BoldText} from './BoldText';
import {NormalText} from './NormalText';
import React from 'react';
import {getRenderedTree} from 'src/shared/test-utils/renderedUtils';

describe('<BoldText />', () => {
  it('should render correctly', async () => {
    let rendered: ReactTestRenderer | null = null;

    await act(async () => {
      rendered = renderer.create(<BoldText />);
    });

    expect(getRenderedTree(rendered)).toBeTruthy();
  });
});

describe('<NormalText />', () => {
  it('should render correctly', async () => {
    let rendered: ReactTestRenderer | null = null;

    await act(async () => {
      rendered = renderer.create(<NormalText />);
    });

    expect(getRenderedTree(rendered)).toBeTruthy();
  });
});
