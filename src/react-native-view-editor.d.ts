declare module 'react-native-view-editor' {
  import React from 'react';
  import {View} from 'react-native';

  export default class ViewEditor extends React.Component<{
    style?: View['props']['style'];
    imageHeight: number;
    imageWidth: number;
    imageContainerHeight?: number;
    imageContainerWidth?: number;
    imageMask?: React.ReactNode;
    maskHeight?: number;
    maskWidth?: number;
    maskPadding?: number;
    children?: React.ReactNode;
    rotate?: boolean;
    panning?: boolean;
    center: boolean;
    croppingRequired: boolean;
    imageMaskShown: boolean;
    showAtTop?: boolean;
    useCustomContent?: boolean;
    // used for multi-images
    bigContainerWidth?: number;
    bigContainerHeight?: number;
    requiresMinScale?: boolean;
    initialScale?: number;
    initialPan?: {x: number; y: number};
    initialRotate?: string;
    onPressCallback?: () => void;
    onLongPressCallback?: () => void;
    onLongPressReleaseCallback?: () => void;
    onMoveCallback?: () => void;
    onEndCallback?: () => void;
    onLoad?: () => void;
  }> {}
}
