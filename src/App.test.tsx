import 'react-native';

// Note: test renderer must be required after react-native.
import renderer, {act} from 'react-test-renderer';

import {App} from './App';
import React from 'react';

it('should render correctly', async () => {
  await act(async () => {
    renderer.create(<App />);
  });
});
