import {Image, View, useWindowDimensions} from 'react-native';

import ImageZoom from 'react-native-image-pan-zoom';
import {NavigationProps} from 'src/shared/types/NavigationProps';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StackScreenProps} from '@react-navigation/stack';

type Props = StackScreenProps<NavigationProps, 'Image'>;

export function ImageScreen(props: Props) {
  const {
    route: {params},
    navigation,
  } = props;
  const {width, height} = useWindowDimensions();
  if (!params?.image) {
    navigation.replace('Home');
    return <View />;
  }
  const minScaleWidth = width / params.image.width;
  const minScaleHeight = height / params.image.height;
  const minScale = minScaleWidth < minScaleHeight ? minScaleWidth : minScaleHeight;
  return (
    <SafeAreaView>
      <ImageZoom
        cropWidth={width}
        cropHeight={height}
        imageWidth={params.image.width}
        imageHeight={params.image.height}
        enableCenterFocus={false}
        centerOn={{x: 0, y: 0, scale: minScale, duration: 1}}
        minScale={minScale}
        maxScale={minScale > 1 ? minScale : 1}>
        <Image source={{uri: params.image.url, width: params.image.width, height: params.image.height}} />
      </ImageZoom>
    </SafeAreaView>
  );
}
