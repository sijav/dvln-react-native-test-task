import 'react-native';

import {ImageInterface} from 'src/core/interfaces';
import {ImageScreen} from './ImageScreen';
import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import {createTestProps} from 'src/shared/test-utils/createTestProps.test';

const renderer = ShallowRenderer.createRenderer();

describe('<ImageScreen />', () => {
  it('should render and match the snapshot', async () => {
    const props = createTestProps({});
    renderer.render(<ImageScreen {...props} />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
  it('should render with id', async () => {
    const image: ImageInterface = {
      breeds: [],
      categories: [{id: 1, name: 'hats'}],
      width: 600,
      height: 400,
      url: 'https://26.media.tumblr.com/tumblr_krwvziszMn1qa9hjso1_1280.jpg',
      id: 'g',
    };
    const props = createTestProps({route: {params: {image}}});
    renderer.render(<ImageScreen {...props} />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
