import {Button, useWindowDimensions} from 'react-native';
import React, {useCallback} from 'react';
import {getCategories, getImages} from 'src/core/slices';
import {useDispatch, useSelector} from 'react-redux';

import {DrawerScreenProps} from '@react-navigation/drawer';
import {ImageItems} from './ImageItems';
import {MainStoreState} from 'src/core/stores';
import {NavigationProps} from 'src/shared/types/NavigationProps';
import {NormalText} from 'src/shared/texts';
import {SafeAreaView} from 'react-native-safe-area-context';
import {calculateIsLargeScreen} from 'src/shared/utils/calculateIsLargeScreen';

export function HomeScreen(props: DrawerScreenProps<NavigationProps, 'Home'>) {
  const {
    route: {params},
    navigation,
  } = props;
  const {width} = useWindowDimensions();
  const isLargeScreen = calculateIsLargeScreen(width);
  const handleOpenDrawer = useCallback(() => {
    navigation.openDrawer();
  }, [navigation]);
  const {imagesError, categoriesError} = useSelector((state: MainStoreState) => ({
    imagesError: state.images.errorMessage,
    categoriesError: state.categories.errorMessage,
  }));
  const dispatch = useDispatch();
  const handleRefreshImages = useCallback(() => params?.id && dispatch(getImages(params.id)), [dispatch, params]);
  const handleRefreshCategories = useCallback(() => dispatch(getCategories()), [dispatch]);
  return (
    <SafeAreaView>
      {params?.id ? (
        imagesError ? (
          <>
            <NormalText>{imagesError}</NormalText>
            <Button color="#12D4C1" title="Try again?" onPress={handleRefreshImages} />
          </>
        ) : (
          <ImageItems categoryId={params.id} navigation={navigation} />
        )
      ) : categoriesError ? (
        <>
          <NormalText>{categoriesError}</NormalText>
          <Button color="#12D4C1" title="Try again?" onPress={handleRefreshCategories} />
        </>
      ) : (
        <>
          <NormalText>Please select one of the category from the Drawer</NormalText>
          {!isLargeScreen && <Button color="#12D4C1" title="Open drawer" onPress={handleOpenDrawer} />}
        </>
      )}
    </SafeAreaView>
  );
}
