import styled from 'styled-components/native';

interface ImageItemWrapperProps {
  isDark?: boolean;
}

export const ImageItemWrapper = styled.View`
  margin: 10px
  background: ${(props: ImageItemWrapperProps) => (props.isDark ? '#666' : '#eee')}
  borderRadius: 10px
  overflow: hidden
  elevation: 5
  shadowColor: #000
  shadowOffset: 0 0
  shadowOpacity: 0.1
  shadowRadius: 10px
`;
