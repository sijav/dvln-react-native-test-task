import 'react-native';

import {HomeScreen} from './HomeScreen';
import {Provider} from 'react-redux';
import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import {createTestProps} from 'src/shared/test-utils/createTestProps.test';
import {store} from 'src/core/stores';

const renderer = ShallowRenderer.createRenderer();

describe('<HomeScreen />', () => {
  it('should render and match the snapshot', async () => {
    const props = createTestProps({});
    renderer.render(
      <Provider store={store}>
        <HomeScreen {...props} />
      </Provider>,
    );
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });

  it('should render with id', async () => {
    const props = createTestProps({route: {params: {id: 1}}});
    renderer.render(
      <Provider store={store}>
        <HomeScreen {...props} />
      </Provider>,
    );
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
