import styled from 'styled-components/native';

export const ImagesWrapper = styled.View`
  flex: 1
  flexDirection: row
  alignItems: center
  flexWrap: wrap
`;
