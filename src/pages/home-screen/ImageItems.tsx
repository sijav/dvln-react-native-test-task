import {Image, useColorScheme} from 'react-native';
import React, {useEffect} from 'react';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';

import {DrawerNavigationProp} from '@react-navigation/drawer';
import {ImageItemWrapper} from './ImageItemWrapper';
import {ImagePlaceHolder} from 'src/shared/placeholder';
import {ImagesWrapper} from './ImagesWrapper';
import {MainStoreState} from 'src/core/stores';
import {NavigationProps} from 'src/shared/types/NavigationProps';
import {getImages} from 'src/core/slices';
import {useScreenViewSize} from 'src/shared/utils/useScreenViewSize';

interface OwnProps {
  categoryId: number;
  navigation: DrawerNavigationProp<NavigationProps>;
}

type Props = OwnProps;

export function ImageItems(props: Props) {
  const {categoryId, navigation} = props;
  const color = useColorScheme();
  const isDark = color === 'dark';
  const dispatch = useDispatch();
  const {data, isLoading} = useSelector((state: MainStoreState) => state.images);
  useEffect(() => {
    dispatch(getImages(categoryId));
  }, [dispatch, categoryId]);
  const {screenHeight, screenWidth} = useScreenViewSize();
  const maxSize = Math.min(screenHeight - 20, screenWidth / 2 - 20, 200);
  return (
    <ScrollView>
      {isLoading ? (
        <ImagePlaceHolder count={8} />
      ) : (
        <ImagesWrapper>
          {data.map((image) => (
            <TouchableOpacity key={image.id} onPress={() => navigation.navigate('Image', {image: image})}>
              <ImageItemWrapper isDark={isDark}>
                <Image source={{uri: image.url, height: maxSize, width: maxSize}} />
              </ImageItemWrapper>
            </TouchableOpacity>
          ))}
        </ImagesWrapper>
      )}
    </ScrollView>
  );
}
