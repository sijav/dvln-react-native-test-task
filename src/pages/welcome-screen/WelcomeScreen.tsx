import {BoldText, NormalText} from 'src/shared/texts';
import {Button, Image, Linking, useColorScheme, useWindowDimensions} from 'react-native';

import {BackgroundImage} from 'src/shared/images';
import {InfoTextWrapper} from './InfoTextWrapper';
import {NavigationProps} from 'src/shared/types/NavigationProps';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';
import {StackScreenProps} from '@react-navigation/stack';
import {StartWrapper} from './StartWrapper';
import {TextWrapper} from 'src/shared/text-wrapper';
import concept from 'src/assets/images/concept.png';
import darkConcept from 'src/assets/images/concept-dark.png';
import logo from 'src/assets/images/logo.png';

type Props = StackScreenProps<NavigationProps>;

export function WelcomeScreen(props: Props) {
  const {height} = useWindowDimensions();
  const color = useColorScheme();
  return (
    <SafeAreaView>
      <BackgroundImage source={color === 'dark' ? darkConcept : concept} resizeMode="cover" />
      <ScrollView style={{minHeight: height}}>
        <TextWrapper>
          <NormalText>Hello</NormalText>
          <Image source={logo} resizeMode="contain" />
          <BoldText>React Native Test Task</BoldText>

          <NormalText>
            Here's a test case of a simple React-Redux React Native Application to show my hand writing codes and to
            show how I use components
          </NormalText>

          <InfoTextWrapper>
            <NormalText>Here's my</NormalText>
            <Button
              color="#12D4C1"
              title="LinkedIn"
              onPress={() => {
                Linking.openURL('https://www.linkedin.com/in/sijav');
              }}
            />
          </InfoTextWrapper>
          <StartWrapper>
            <NormalText>Ready to explore 🚀</NormalText>
            <Button color="#12D4C1" title="Start" onPress={() => props.navigation.replace('Home')} />
          </StartWrapper>
        </TextWrapper>
      </ScrollView>
    </SafeAreaView>
  );
}
