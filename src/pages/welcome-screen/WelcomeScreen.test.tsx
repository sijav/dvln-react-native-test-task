import 'react-native';

import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import {WelcomeScreen} from './WelcomeScreen';
import {createTestProps} from 'src/shared/test-utils/createTestProps.test';

const renderer = ShallowRenderer.createRenderer();

describe('<WelcomeScreen />', () => {
  it('should render and match the snapshot', async () => {
    const props = createTestProps({});
    renderer.render(<WelcomeScreen {...props} />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
