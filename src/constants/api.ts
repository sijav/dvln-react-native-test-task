import {CAT_API} from '@env';
// as CAT_API may not be existed this is necessary
export const MAIN_API = (CAT_API as string) || '';
