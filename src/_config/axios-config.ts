import {MAIN_API} from 'src/constants/api';
import axios from 'axios';

axios.defaults.baseURL = MAIN_API; // by default it's a String;
