import 'react-native';

import {MainContainer} from './MainContainer';
import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

const renderer = ShallowRenderer.createRenderer();

describe('<MainContainer />', () => {
  it('should render and match the snapshot', async () => {
    renderer.render(<MainContainer />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
