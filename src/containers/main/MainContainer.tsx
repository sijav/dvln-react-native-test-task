import {MainRouteStacks} from './MainRouteStacks';
import {NavigationContainer} from '@react-navigation/native';
import React from 'react';

export function MainContainer() {
  return (
    <NavigationContainer>
      <MainRouteStacks />
    </NavigationContainer>
  );
}
