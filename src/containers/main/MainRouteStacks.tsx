import {Pressable, StyleProp, ViewStyle, useColorScheme} from 'react-native';
import React, {useCallback} from 'react';

import {DrawerActions} from '@react-navigation/native';
import {HomeContainer} from 'src/containers/home';
import {ImageScreen} from 'src/pages/image-screen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {MainStoreState} from 'src/core/stores';
import {SafeAreaView} from 'react-native-safe-area-context';
import {WelcomeScreen} from 'src/pages/welcome-screen';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';

const Stack = createStackNavigator();

export function MainRouteStacks() {
  const color = useColorScheme();

  const {data} = useSelector((state: MainStoreState) => state.categories);

  const tintColor = color === 'dark' ? 'white' : 'black';

  const homeScreenOptions = useCallback(
    // props is any because we're trying to get state of route which is restricted already
    // TODO: if found a better way
    (props: any) => {
      const headerLeft = () => (
        <SafeAreaView>
          <Pressable
            onPress={() => props.navigation.dispatch(DrawerActions.toggleDrawer())}
            style={iconTouchableOpacityStyle}
            android_ripple={{
              color: tintColor,
              radius: 20,
            }}>
            <Ionicons name="menu" color={tintColor} size={25} />
          </Pressable>
        </SafeAreaView>
      );
      try {
        // a hackish way to get nested params
        // TODO: there should be probably a better way
        const id = props?.route?.state?.routes?.[0]?.params?.id as number;
        return {
          title: id ? data.find((category) => category.id === id)?.name : 'Home',
          headerLeft,
        };
      } catch {
        return {title: 'Home', headerLeft};
      }
    },
    [data, tintColor],
  );

  return (
    <Stack.Navigator initialRouteName="Welcome" screenOptions={{headerTintColor: tintColor}}>
      <Stack.Screen name="Welcome" component={WelcomeScreen} options={{headerShown: false}} />
      <Stack.Screen name="Home" component={HomeContainer} options={homeScreenOptions} />
      <Stack.Screen name="Image" component={ImageScreen} />
    </Stack.Navigator>
  );
}

const iconTouchableOpacityStyle: StyleProp<ViewStyle> = {
  padding: 20,
};
