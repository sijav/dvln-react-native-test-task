import {DrawerContentComponentProps, DrawerContentOptions, DrawerItem} from '@react-navigation/drawer';
import React, {useEffect} from 'react';
import {ScrollView, StyleProp, TextStyle} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Animated from 'react-native-reanimated';
import {ItemPlaceHolder} from 'src/shared/placeholder';
import {MainStoreState} from 'src/core/stores';
import {getCategories} from 'src/core/slices';

export function DrawerContent(props: DrawerContentComponentProps<DrawerContentOptions>) {
  const {navigation, progress} = props;
  const dispatch = useDispatch();
  const {data, isLoading} = useSelector((state: MainStoreState) => state.categories);
  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);
  const translateX = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [-100, 0],
  });

  return (
    <Animated.View style={{transform: [{translateX}]}}>
      <ScrollView {...props}>
        {!isLoading ? (
          data.map((cat) => (
            <DrawerItem
              key={cat.id}
              label={cat.name}
              labelStyle={drawerItemStyle}
              onPress={() => {
                navigation.navigate('Home', {id: cat.id});
              }}
            />
          ))
        ) : (
          <ItemPlaceHolder count={8} />
        )}
      </ScrollView>
    </Animated.View>
  );
}

const drawerItemStyle: StyleProp<TextStyle> = {
  fontSize: 18,
};
