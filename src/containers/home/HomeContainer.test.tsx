import 'react-native';

import {HomeContainer} from './HomeContainer';
import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

const renderer = ShallowRenderer.createRenderer();

describe('<HomeContainer />', () => {
  it('should render and match the snapshot', async () => {
    renderer.render(<HomeContainer />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
