import React, {useCallback} from 'react';

import {DrawerContent} from './DrawerContent';
import {HomeScreen} from 'src/pages/home-screen';
import {calculateIsLargeScreen} from 'src/shared/utils/calculateIsLargeScreen';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {useWindowDimensions} from 'react-native';

const Drawer = createDrawerNavigator();

export function HomeContainer() {
  const {width} = useWindowDimensions();
  const isLargeScreen = calculateIsLargeScreen(width);
  const handleDrawerContent = useCallback((props) => <DrawerContent {...props} />, []);

  return (
    <Drawer.Navigator
      drawerContent={handleDrawerContent}
      openByDefault
      initialRouteName="Home"
      drawerType={isLargeScreen ? 'permanent' : 'front'}>
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        // TODO: this option does not work for top bar
        // options={{
        //   drawerLabel: () => 'Category Name',
        //   title: 'Something',
        //   drawerIcon: (props) =>  <Ionicons size={props.size} color={props.color} name="menu" />,
        // }}
      />
    </Drawer.Navigator>
  );
}
