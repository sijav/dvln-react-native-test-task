declare module 'react-native-svg-animated-linear-gradient' {
  import {SvgProps, rgbaArray} from 'react-native-svg';

  import React from 'react';

  export interface ReactNativeSvgAnimatedLinearGradientProps {
    duration?: number;
    useNativeDriver?: boolean;
    x1?: string | number;
    y1?: string | number;
    x2?: string | number;
    y2?: string | number;
    primaryColor?: string | number | rgbaArray;
    secondaryColor?: string | number | rgbaArray;
  }
  export default class ReactNativeSvgAnimatedLinearGradient extends React.Component<
    SvgProps & ReactNativeSvgAnimatedLinearGradientProps
  > {}
}
