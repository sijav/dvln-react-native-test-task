/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import './_config';

import {MainContainer} from './containers/main';
import {Provider} from 'react-redux';
import React from 'react';
import {store} from './core/stores';

export function App() {
  return (
    <Provider store={store}>
      <MainContainer />
    </Provider>
  );
}
