import {categoriesReducer} from './categories';
import {combineReducers} from 'redux';
import {imagesReducer} from './images';

export const reducers = combineReducers({
  categories: categoriesReducer,
  images: imagesReducer,
});
