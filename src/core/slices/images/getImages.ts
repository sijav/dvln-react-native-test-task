import {ImagesState, imagesActions} from './images-slice';

import {CommonThunkAction} from 'src/shared/types/CommonThunkAction';
import {ImageInterface} from 'src/core/interfaces';
import axios from 'axios';

export const getImages = (categoryId: number): CommonThunkAction<ImagesState> => async (dispatch) => {
  dispatch(imagesActions.request());
  try {
    const response = await axios.get<ImageInterface[]>('/images/search', {
      params: {
        limit: 10,
        category_ids: categoryId,
      },
    });
    dispatch(imagesActions.set(response.data));
  } catch (err) {
    dispatch(imagesActions.error(err?.response?.data ?? err?.message ?? 'An error occurred, Please try again later'));
  }
};
