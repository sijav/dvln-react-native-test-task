import {PayloadAction, createSlice} from '@reduxjs/toolkit';

import {ImageInterface} from 'src/core/interfaces';

export type ImagesState = {
  data: ImageInterface[];
  errorMessage: string | null;
  isLoading: boolean;
};

export const initialState: ImagesState = {
  data: [],
  errorMessage: null,
  isLoading: false,
};

const imagesSlices = createSlice({
  name: 'images',
  initialState,
  reducers: {
    request: () => ({isLoading: true, errorMessage: null, data: []}),
    set: (state, action: PayloadAction<ImageInterface[]>) => ({
      errorMessage: null,
      isLoading: false,
      data: action.payload,
    }),
    error: (state, action: PayloadAction<string>) => ({isLoading: false, errorMessage: action.payload, data: []}),
  },
});

export const imagesReducer = imagesSlices.reducer;
export const imagesActions = imagesSlices.actions;
