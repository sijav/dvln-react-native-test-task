import {CategoryInterface, ImageInterface} from 'src/core/interfaces';
import {imagesActions, imagesReducer, initialState} from './images-slice';

import {getImages} from './getImages';
import {mockAxios} from 'src/shared/test-utils/mockAxios.test';
import {mockStore} from 'src/shared/test-utils/mockStore.test';

describe('Images slice', () => {
  describe('reducer, actions and selectors', () => {
    it('should return the initial state on first run', () => {
      // Arrange
      const nextState = initialState;

      // Act
      const result = imagesReducer(undefined, {} as any);

      // Assert
      expect(result).toEqual(nextState);
    });

    it('should properly set the state when request happens', () => {
      // Act
      const nextState = imagesReducer(initialState, imagesActions.request());

      // Assert
      expect(nextState.errorMessage).toBe(null);
      expect(nextState.isLoading).toBe(true);
      expect(nextState.data).toEqual([]);
    });

    it('should properly set the state when error ocoured', () => {
      // Arrange
      const errorMessage = 'Some random error';

      // Act
      const nextState = imagesReducer(initialState, imagesActions.error(errorMessage));

      // Assert
      expect(nextState.errorMessage).toEqual(errorMessage);
      expect(nextState.isLoading).toBe(false);
      expect(nextState.data).toEqual([]);
    });

    it('should properly set the state on setting the data', () => {
      // Arrange
      const category: CategoryInterface = {id: 1, name: 'name1'};
      const data: ImageInterface[] = [
        {
          breeds: [],
          categories: [category],
          height: 200,
          width: 200,
          url: 'https://picsum.photos/200',
          id: 'A',
        },
      ];

      // Act
      const nextState = imagesReducer(initialState, imagesActions.set(data));

      // Assert
      expect(nextState.errorMessage).toBe(null);
      expect(nextState.isLoading).toBe(false);
      expect(nextState.data).toEqual(data);
    });
  });

  describe('thunks', () => {
    afterEach(() => {
      mockAxios.reset();
    });

    it('creates data when fetch succeeds', async () => {
      // Arrange
      const category: CategoryInterface = {id: 1, name: 'name1'};
      const url = '/images/search';
      const responseData: ImageInterface[] = [
        {
          breeds: [],
          categories: [category],
          height: 200,
          width: 200,
          url: 'https://picsum.photos/200',
          id: 'A',
        },
      ];
      const responseHeaders = {'content-type': 'application/json'};
      mockAxios.onGet(url).replyOnce(200, responseData, responseHeaders);
      const store = mockStore(initialState);

      // Act
      await store.dispatch(getImages(category.id) as any); // as any because of the type error between AnyAction and ThunkAction

      // Assert
      const expectedActions = [imagesActions.request(), imagesActions.set(responseData)];
      const expectedParams = {limit: 10, category_ids: category.id};

      expect(mockAxios.history.get.length).toBe(1);
      expect(mockAxios.history.get[0].params).toEqual(expectedParams);
      expect(mockAxios.history.get[0].url?.endsWith(url)).toBe(true);
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('handles error on rejected', async () => {
      // Arrange
      const category: CategoryInterface = {id: 1, name: 'name1'};
      const errorMessage = 'Network Error';
      const url = '/images/search';
      mockAxios.onGet(url).networkErrorOnce();
      const store = mockStore(initialState);

      // Act
      await store.dispatch(getImages(category.id) as any); // as any because of the type error between AnyAction and ThunkAction

      // Assert
      const expectedActions = [imagesActions.request(), imagesActions.error(errorMessage)];
      const expectedParams = {limit: 10, category_ids: category.id};

      expect(mockAxios.history.get.length).toBe(1);
      expect(mockAxios.history.get[0].params).toEqual(expectedParams);
      expect(mockAxios.history.get[0].url?.endsWith(url)).toBe(true);
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
