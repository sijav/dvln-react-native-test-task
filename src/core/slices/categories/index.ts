export {categoriesReducer, categoriesActions} from './categories-slice';
export {getCategories} from './getCategories';
