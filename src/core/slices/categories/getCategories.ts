import {CategoriesState, categoriesActions} from './categories-slice';

import {CategoryInterface} from 'src/core/interfaces';
import {CommonThunkAction} from 'src/shared/types/CommonThunkAction';
import axios from 'axios';

export const getCategories = (): CommonThunkAction<CategoriesState> => async (dispatch) => {
  dispatch(categoriesActions.request());
  try {
    const response = await axios.get<CategoryInterface[]>('/categories');
    dispatch(categoriesActions.set(response.data));
  } catch (err) {
    dispatch(
      categoriesActions.error(err?.response?.data ?? err?.message ?? 'An error occurred, Please try again later'),
    );
  }
};
