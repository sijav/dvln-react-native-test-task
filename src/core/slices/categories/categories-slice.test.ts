import {categoriesActions, categoriesReducer, initialState} from './categories-slice';

import {CategoryInterface} from 'src/core/interfaces';
import {getCategories} from 'src/core/slices/thunks';
import {mockAxios} from 'src/shared/test-utils/mockAxios.test';
import {mockStore} from 'src/shared/test-utils/mockStore.test';

describe('Categories slice', () => {
  describe('reducer, actions and selectors', () => {
    it('should return the initial state on first run', () => {
      // Arrange
      const nextState = initialState;

      // Act
      const result = categoriesReducer(undefined, {} as any);

      // Assert
      expect(result).toEqual(nextState);
    });

    it('should properly set the state when request happens', () => {
      // Act
      const nextState = categoriesReducer(initialState, categoriesActions.request());

      // Assert
      expect(nextState.errorMessage).toEqual(null);
      expect(nextState.isLoading).toEqual(true);
      expect(nextState.data).toEqual([]);
    });

    it('should properly set the state when error ocoured', () => {
      // Arrange
      const errorMessage = 'Some random error';

      // Act
      const nextState = categoriesReducer(initialState, categoriesActions.error(errorMessage));

      // Assert
      expect(nextState.errorMessage).toEqual(errorMessage);
      expect(nextState.isLoading).toEqual(false);
      expect(nextState.data).toEqual([]);
    });

    it('should properly set the state on setting the data', () => {
      // Arrange
      const data: CategoryInterface[] = [
        {id: 1, name: 'name1'},
        {id: 2, name: 'name2'},
      ];

      // Act
      const nextState = categoriesReducer(initialState, categoriesActions.set(data));

      // Assert
      expect(nextState.errorMessage).toEqual(null);
      expect(nextState.isLoading).toEqual(false);
      expect(nextState.data).toEqual(data);
    });
  });

  describe('thunks', () => {
    afterEach(() => {
      mockAxios.reset();
    });

    it('creates data when fetch succeeds', async () => {
      // Arrange
      const url = '/categories';
      const responseData = [
        {id: 1, name: 'name1'},
        {id: 2, name: 'name2'},
      ] as CategoryInterface[];
      const responseHeaders = {'content-type': 'application/json'};
      mockAxios.onGet(url).replyOnce(200, responseData, responseHeaders);
      const store = mockStore(initialState);

      // Act
      await store.dispatch(getCategories() as any); // as any because of the type error between AnyAction and ThunkAction

      // Assert
      const expectedActions = [categoriesActions.request(), categoriesActions.set(responseData)];

      expect(mockAxios.history.get.length).toBe(1);
      expect(mockAxios.history.get[0].url?.endsWith(url)).toBe(true);
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('handles error on rejected', async () => {
      // Arrange
      const errorMessage = 'Network Error';
      const url = '/categories';
      mockAxios.onGet(url).networkErrorOnce();
      const store = mockStore(initialState);

      // Act
      await store.dispatch(getCategories() as any); // as any because of the type error between AnyAction and ThunkAction

      // Assert
      const expectedActions = [categoriesActions.request(), categoriesActions.error(errorMessage)];

      expect(mockAxios.history.get.length).toBe(1);
      expect(mockAxios.history.get[0].url?.endsWith(url)).toBe(true);
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
