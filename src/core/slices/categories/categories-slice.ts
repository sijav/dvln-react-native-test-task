import {PayloadAction, createSlice} from '@reduxjs/toolkit';

import {CategoryInterface} from 'src/core/interfaces';

export type CategoriesState = {
  data: CategoryInterface[];
  errorMessage: string | null;
  isLoading: boolean;
};

export const initialState: CategoriesState = {
  data: [],
  errorMessage: null,
  isLoading: false,
};

const categoriesSlices = createSlice({
  name: 'categories',
  initialState,
  reducers: {
    request: () => ({isLoading: true, errorMessage: null, data: []}),
    set: (state, action: PayloadAction<CategoryInterface[]>) => ({
      errorMessage: null,
      isLoading: false,
      data: action.payload,
    }),
    error: (state, action: PayloadAction<string>) => ({isLoading: false, errorMessage: action.payload, data: []}),
  },
});

export const categoriesReducer = categoriesSlices.reducer;
export const categoriesActions = categoriesSlices.actions;
