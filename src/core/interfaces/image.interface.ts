import {CategoryInterface} from './category.interface';

export interface ImageInterface {
  breeds: [];
  categories: CategoryInterface[];
  id: string;
  url: string;
  width: number;
  height: number;
}
