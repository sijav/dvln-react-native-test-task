import {configureStore} from '@reduxjs/toolkit';
import {reducers} from 'src/core/slices/reducers';
import thunk from 'redux-thunk';

export const store = configureStore({
  reducer: reducers,
  middleware: [thunk],
});

export type MainStoreType = typeof store.dispatch;
export type MainStoreState = ReturnType<typeof reducers>;
