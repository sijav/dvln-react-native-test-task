/* eslint-disable no-undef */
import 'react-native/Libraries/Animated/src/bezier'; // for https://github.com/facebook/jest/issues/4710

jest.mock('react-native-gesture-handler', () => {
  const View = require('react-native/Libraries/Components/View/View');
  return {
    Swipeable: View,
    DrawerLayout: View,
    State: {},
    ScrollView: View,
    Slider: View,
    Switch: View,
    TextInput: View,
    ToolbarAndroid: View,
    ViewPagerAndroid: View,
    DrawerLayoutAndroid: View,
    WebView: View,
    NativeViewGestureHandler: View,
    TapGestureHandler: View,
    FlingGestureHandler: View,
    ForceTouchGestureHandler: View,
    LongPressGestureHandler: View,
    PanGestureHandler: View,
    PinchGestureHandler: View,
    RotationGestureHandler: View,
    /* Buttons */
    RawButton: View,
    BaseButton: View,
    RectButton: View,
    BorderlessButton: View,
    /* Other */
    FlatList: View,
    gestureHandlerRootHOC: jest.fn(), // for gesture handler warning
    Directions: {},
  };
});
jest.mock('react-native-reanimated', () => require('react-native-reanimated/mock')); // for reanimated module error
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper'); // for useNativeDriver animations warning
