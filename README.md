<h1 align="center">
  <a href="https://gitlab.com/sijav/dvln-react-native-test-task/">
    React Native Test Task (Cats)
  </a>
</h1>

<p align="center">
  <strong>Learn once, write anywhere:</strong><br>
  The cat's project test case with react native.
</p>

<h3 align="center">
  <a href="https://reactnative.dev/docs/getting-started">Getting Started</a>
  <span> · </span>
  <a href="https://reactnative.dev/docs/tutorial">Learn the Basics</a>
  <span> · </span>
  <a href="https://reactnative.dev/showcase.html">Showcase</a>
  <span> · </span>
  <a href="https://reactnative.dev/docs/contributing">Contribute</a>
  <span> · </span>
  <a href="https://reactnative.dev/en/help">Community</a>
  <span> · </span>
  <a href="https://github.com/facebook/react-native/blob/master/.github/SUPPORT.md">Support</a>
</h3>

React Native brings [**React**'s][r] declarative UI framework to iOS and Android. With React Native, you use native UI controls and have full access to the native platform.
This app shows a test case with react native and will show you some cats 😻.

- **Declarative.** React makes it painless to create interactive UIs. Declarative views make your code more predictable and easier to debug.
- **Component-Based.** Build encapsulated components that manage their state, then compose them to make complex UIs.
- **Developer Velocity.** See local changes in seconds. Changes to JavaScript code can be live reloaded without rebuilding the native app.
- **Portability.** Reuse code across iOS, Android, and [other platforms][p].

React Native is developed and supported by many companies and individual core contributors. Find out more in our [ecosystem overview][e].

[r]: https://reactjs.org/
[p]: https://reactnative.dev/docs/out-of-tree-platforms
[e]: https://github.com/facebook/react-native/blob/master/ECOSYSTEM.md

## Contents

- [Requirements](#-requirements)
- [Getting Started](#-getting-started)
- [Available Scripts](#-available-scripts)
- [Documentation](#-documentation)

## 🎆 Requirements

React Native apps may target iOS 10.0 and Android 4.1 (API 16) or newer. You may use Windows, macOS, or Linux as your development operating system, though building and running iOS apps is limited to macOS. Tools like [Expo](https://expo.io) can be used to work around this.

## 🎉 Getting Started

- Follow the [Getting Started guide (select the React Native CLI Quickstart)](https://reactnative.dev/docs/getting-started.html) until the Creating a new application guid.
- Then install yarn and run 'yarn' on terminal in the project directory to install node dependencies

## ✏ Available Scripts

In the project directory, you can run:

### `yarn android`

Runs the app in available android device or emulator on development mode.<br />

### `yarn ios`

Runs the app in available ios device or emulator on development mode. (MacOS only)<br />

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:8081](http://localhost:8081) you must have ran the app on device or emulator already.

### `yarn lint`

Lint the whole project to check for errors.<br />

### `yarn sort`

Sort all imports in js and jsx files.<br />
Be sure to run yarn format after this script<br />

**Note: `yarn sort:check` will check sortiness of all files!**

### `yarn format`

Format all js jsx and json files through prettier.<br />
**Note: `yarn format:check` will check for formatness of all files!**

## 📖 Documentation

The full documentation for React Native can be found on react native's official [website][docs].

The React Native documentation discusses components, APIs, and topics that are specific to React Native. For further documentation on the React API that is shared between React Native and React DOM, refer to the [React documentation][r-docs].

The source for the React Native documentation and website is hosted on a separate repo, [**@facebook/react-native-website**][repo-website].

[docs]: https://reactnative.dev/docs/getting-started.html
[r-docs]: https://reactjs.org/docs/getting-started.html
[repo-website]: https://github.com/facebook/react-native-website
